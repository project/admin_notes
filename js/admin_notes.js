
Drupal.behaviors.admin_notes = function(context) {
  // Display an additional class to display the 'present' icon
  if(Drupal.settings.admin_notes.note_present) {
    $('#admin-tab-admin_notes-admin_notes').addClass('admin-tab-admin_notes-admin_notes');
  }

  $('form#admin-notes-block-form .admin_notes_submit').click(function(event) {
    // Prevent the default link action - we don't
    // want to trigger a synchronous response.
    event.preventDefault();
    
    
/* IF THERE IS A BETTER WAY TO IMPLEMENT THIS SECTION THAT YOU KNOW OF PLEASE SUBMIT A PATCH!! */

    // We need to get the current location that we are on
    var loc = window.location.toString();
    // Now we need to strip away the first part of the location
    // For example if our path (location from above) is http://www.example.com/path/1/2/3
    // we want to remove http://www.example.com
    var domainAddress = window.location.href.match(/(https{0,1}:\/\/[0-9a-z\.]*)/)[1];
    // Now we get the rest of the current path, excluding the domain name
    var relativePath = loc.replace(domainAddress, '');
/*  END  */    

    // In case Drupal is in a subdirectory remove it from the path stored in the database
    adminNotesPath = relativePath.replace(Drupal.settings.basePath, '');

    // If the adminNotesPath is blank (because activePath and basePath above are the same)
    // then we are on the homepage and we need to manually set the path
    if (adminNotesPath == '') {  adminNotesPath = '/';  }
    $.ajax({
      type: "GET", // Should this be changed to POST...or does it matter?
      url: Drupal.settings.basePath + "admin_notes/submit", // Using basePath in case Drupal is in a subdirectory
      data: {
        'note' : $('form#admin-notes-block-form #edit-admin-note').val(), // Get value of textbox
        'path' : adminNotesPath
      },
      success: function(msg){
        // If admin is deleting note (i.e. there is nothing in the textbox)
        if ($('form#admin-notes-block-form #edit-admin-note').val() == '') {
          notification = 'Admin Note Deleted';
          // Remove class to the admin notes tab to hide the 'note present' icon
          $('#admin-tab-admin_notes-admin_notes').removeClass('admin-tab-admin_notes-admin_notes');
        }
        else {
          notification = 'Admin Note Saved';
          // Add class to the admin notes tab to show the 'note present' icon
          $('#admin-tab-admin_notes-admin_notes').addClass('admin-tab-admin_notes-admin_notes');
        }
        // Add the message to the Admin Notes area
        $('#edit-admin-note-wrapper').prepend('<div class="messages status">' + notification + '</div>');
        
        // Hide the message after a bit of time
        // Using .animate to 'delay' the fadeOut effect
        $('#edit-admin-note-wrapper .messages').animate({opacity: 1.0}, 3000).fadeOut('slow');
        
        eval(msg);
      }
    });
  });
};


